# adex-backend-django

Click on one of the `adex_backend` links for more details about the API.

## Development
* pycharm  : http://localhost:8000/adex_backend/
* WSL-2: http://localhost/adex_backend/

## Test Environment
* sdc-dev: https://sdc-dev.astron.nl/adex_backend/
* SURF: http://145.38.187.31/adex_backend/
* [Deployment Diagram](https://drive.google.com/file/d/1uW_mzp5QWJzmihQh2xELUITvKDCor-31/view)

## Production Environment
* https://sdc.astron.nl/adex_backend/

## adex-next
ADEX Frontend under development
* https://gitlab.com/astron-sdc/adex

## adex-labs
ADEX engineering frontend
* https://git.astron.nl/astron-sdc/adex-labs

## Documentation
* [ADEX documentation matrix](https://support.astron.nl/confluence/pages/viewpage.action?pageId=73556208)
* [backend implementation plan](https://support.astron.nl/confluence/display/SDCP/ADEX+Backend+Implementation+Plan)
* [ADEX Deployment Document](https://support.astron.nl/confluence/display/SDCP/ADEX+Deployment)

### Handling database changes

#### in local development environment :
  * make the change in models.py
  * create migration file : ```python manage.py makemigrations --settings=adex_backend.settings.dev```
  * migrate local database: ```python manage.py migrate --settings=adex_backend.settings.dev```
  * git commit new migration file
  * update the data model diagram

#### after deployment on test servers that do not have the automatic migration step:

Migrate the database on all the servers where adex-backend is deployed, currently:
  * optional: WSL-2 (localhost): 
    * ```docker exec -it adex-backend-django python manage.py migrate --settings adex_backend.settings.docker```
  * optional: SURF (personal test machine), 
    * log into the container: ```sudo docker exec -it adex-backend-django bash```, 
    * then: ```python manage.py migrate --settings adex_backend.settings.docker```

