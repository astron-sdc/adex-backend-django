# ADEX Deployment Guide

This guide mainly describes how ADEX can be deployed for 'bare metal' local development.
This means that Docker is not assumed and the code can be run on the commandline or in an IDE.

Additionally, there is a short suggestion about how ADEX can be deployed in production in Docker containers using CI/CD pipelines.

## ADEX Architecture
ADEX is divided in 3 distinct components which can be deployed and developed separately.

This 'separation of concerns' makes it possible to reduce the overall complexity by keeping specific functionality and technology confined to the component that needs it. 
It is also possible to exchange components as long as they honor the expected interfaces. 
For example we have experimented with several frontends, and even an alternative 'fastapi' backend, using the same interfaces.

The **adex-data-scraper** component is meant to be expanded to access additional datasources when the need arises. 
Additional 'connectors' can be written for that. Writing connectors is not in scope of the deployment guide, 
but an example of using a VO 'connector' to import data is given in section `3.0 - adex-data-scraper`.

![](adex_architecture.png)

The following table lists the 3 ADEX components.

| component    | type | repository | function                                                                                                                                                                                                             | technologies |
| -------- | ------- | -------- |----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------| -------- |
| adex backend  | server side web application, (REST API).   |  https://git.astron.nl/astron-sdc/adex-backend-django   | <ul><li>a fast cache database containing a simplified generic datamodel that can contain a diverse range of dataproducts.</li><li>  a REST API to access these dataproducts and some specific functionality.</li></ul> | Django, DRF, Postgres    |
| adex frontend | client side web application (GUI)   | https://gitlab.com/astron-sdc/adex   | GUI to access the dataproducts through a 'skyview'                                                                                                                                                                   | ReactJS/Typescript, Aladin Light    |
| adex-data-scraper    | pip installable python application, currently run manually from CLI.   | https://git.astron.nl/astron-sdc/adex-data-scraper   | <ul><li>accessing (scraping) VO and SQL datasources to read its dataproducts</li><li>logic to translate this varied set of datasources into the generic ADEX datamodel</li><li>logic to control the flow of the scraping and updating of dataproducts</li></ul>                                                                                                             | Python, SQL (Postgres, Oracle), VO   |



<br>

# Deployment for development (bare metal)

## 1.0 - ADEX backend (adex-backend-django)

The **adex-backend-django** application runs on a web server, and it needs access to a Postgres database containing the **adex-cache** database.
Django comes with a development webserver (gunicorn) which will be installed as part of this deployment.

A Postgres database server is not installed. 
If you do run Docker, it is easy to spin up a postgres container (we use the `postgres:14` image).
Otherwise a local 'bare metal' installation of Postgres will do.


prerequisites:
* git, pip and virtualenv installed
* Postgres running

### STEP 1.1 - Create Postgres database
You first need to create a database in Progres. 
The database should mirror the settings and credentials as defined in _settings/dev.txt_ of the application. 
You can change that if needed, but make sure that the settings in Postgres and _settings/dev.txt_ match.
This installation guide assumes the default settings.

In Postgres, create the **adex_cache_django** database for user _**postgres**_ with password _**postgres**_.
(you can use a GUI like `pgadmin` for that, or use `psql` from the commandline)

```json
DATABASES = {
    'default': {
         'ENGINE': 'django.db.backends.postgresql_psycopg2',
         'NAME': 'adex_cache_django',
         'HOST': 'localhost',
         'PORT': '5432',
         'USER': 'postgres',
         'PASSWORD': 'postgres',
    },
}
```

### STEP 1.2 - Install adex-backend-django
clone the repo and install the code in a virtualenv.
Depending on your OS, the 'virtualenv' command may slightly differ.
This is how it works on Ubuntu Linux.

```commandline
> git clone https://git.astron.nl/astron-sdc/adex-backend-django
> cd adex-backend-django
> virtualenv -p python3 venv
> source venv/bin/activate
> cd adex_backend
> pip install -r requirements/base.txt
> python manage.py migrate --settings=adex_backend.settings.dev
```

### STEP 1.3 - Run adex-backend-django

```commandline
> python manage.py runserver --settings=adex_backend.settings.dev

Django version 4.1.3, using settings 'adex_backend.settings.dev'
Starting development server at http://127.0.0.1:8000/
Quit the server with CONTROL-C.
```

You should now get the message that the backend is running and you can access it at: http://localhost:8000/adex_backend

The basic GUI that you now see explains the REST API which can be consumed by a frontend. You can also click on the links to explore the API endpoints manually. 
Note that this GUI is not geared towards the end-user, but towards the developer.

<BR>

![](backend_gui.png)

### STEP 1.4 - Create superuser
A 'superuser' is needed to access the API endpoints that create or update data. (POST and PUT).
Later we also need this 'superuser' to create a token for the **adex-data-scaper** to authenticate with (see step 3.2.1)

> python manage.py createsuperuser --settings=adex_backend.settings.dev

You can fill in your desired username and password, for instance `admin/admin`, because this is only local development. 
Now you can use the following link to access the 'Django admin' page: http://localhost:8000/adex_backend/admin/
There you can access the database, including the user administration.

<BR>

![](django_admin1.png)
![](django_admin2.png)


### STEP 1.5 - Manually add some data (not required)
In step 3.2.3 we will use the **adex-data-scraper** to load data from a VO resource.
You could now skip to STEP 2.0, but manually adding some data first may clarify a bit more about ADEX.

Manually adding some data can be done with the Django admin interface (see screenshots) or with the REST API.
The most simple example is to add 1 _Activity_ and 2 _Primary_ dataproducts. After that the endpoints will show data:

* http://localhost:8000/adex_backend/api/v1/activity/
* http://localhost:8000/adex_backend/api/v1/primary_dp/

![](django_admin3.png)
![](django_admin4.png)

### STEP 1.6 - Configuration
Configuration of the options in the ADEX Frontend are partially done in a configuration file on the ADEX backend.
Note that in the example we filled in '_installation_demo_' as a collection. This collection does not default exist in ADEX, but you can add it to the appropriate configuration file.

This installation guide describes the '**adex-next**' frontend as the frontend of choice, and its configuration file can be found at this location in the **adex-backend-django** repo:
https://git.astron.nl/astron-sdc/adex-backend-django/-/blob/main/adex_backend/adex_backend/configuration/adex-next.py?ref_type=heads

Look for the '_collections_' key and add the section for the '_installation_demo_' to it:

```json
"collections": [
   {"name" : "installation_demo", "dp_types": ["science-skymap"]},
   {"name": "linc_skymap", "dp_types": ["qa-skymap"]},
   {"name": "linc_visibilities", "dp_types": ["die-calibrated-visibilities"], "distinct_field": "dataset_id",},
   {"name": "apertif-dr1", "dp_types": ["science-skymap"]},
   {"name": "lotts-dr2", "dp_types": ["skymap"]},
],
```

You can check the API endpoint if this has worked: http://127.0.0.1:8000/adex_backend/configuration/?config_name=adex-next

<BR>

## 2.0 - ADEX frontend (adex-next)

prerequisites:
* git, node and npm installed

### STEP 2.1: Install adex-next
clone the repo and install the code

```
> git clone https://gitlab.com/astron-sdc/adex
> cd adex
> npm install
> npm run dev

  VITE v4.3.9  ready in 1518 ms

  ➜  Local:   http://127.0.0.1:5173/
  ➜  Network: use --host to expose
  ➜  press h to show help
```

The **adex-next** frontend can now be run from a browser at http://127.0.0.1:5173/

### STEP 2.2: configure the proxy to the backend

Default, the adex-next frontend points at a location on the `sdc.astron.nl` domain where the latest version of the **adex-backend-django** is running. You may want to change that for your local development setup.
You can look up the **vite.config.ts** file in the root directory of the project and edit the 'proxy' setting to point the frontend at the **adex-backend-django** on `localhost:8000` that you have just installed.

```json
export default defineConfig({
  server: {
    proxy: {
      "/adex_backend": "http://127.0.0.1:8000/",
    },
  },
```

The 2 test dataproducts that you entered manually in STEP-5 of the ADEX backend installation now appear in the GUI.
Also, the _installation_demo_ collection that you added to the configuration is now selectable in the GUI.

![](adex_next1.png)

<BR>

## 3.0 - adex-data-scraper
This is a pip installable Python package that can be used to fill the ADEX cache database through the **adex-django-backend** REST API with a bulk of dataproducts from a varied range of datasources.


### STEP 3.1 - install adex-data-scraper
```commandline
> git clone https://git.astron.nl/astron-sdc/adex-data-scraper
> cd adex-data-scraper
> virtualenv -p python3 venv
> source venv/bin/activate
> python -m pip install --upgrade pip
> pip install --upgrade build twine wheel
> python -m build
> pip install .  (note the dot!)
> adex_data_scraper -h
```

Run `adex_data_scraper -h` to check if it works and to see the help.

```commandline
adex_data_scraper -h
usage: adex_data_scraper [-h] [--datasource DATASOURCE] [--data_host DATA_HOST] [--oracle_lib_dir ORACLE_LIB_DIR]
                         [--connector CONNECTOR] [--limit LIMIT] [--batch_size BATCH_SIZE]
                         [--adex_backend_host [ADEX_BACKEND_HOST]] [--adex_resource ADEX_RESOURCE]
                         [--collection COLLECTION] [--clear_resource] [--clear_collection] [--simulate_post]
                         [--adex_token ADEX_TOKEN] [-v] [--argfile [ARGFILE]]

options:
  -h, --help            show this help message and exit
  --datasource DATASOURCE
                        where should the data be imported from? Options: vo, postgres
  --data_host DATA_HOST
                        service/table, either VO or postgres. Examples:
                        https://vo.astron.nl/tap/apertif_dr1.continuum_images, postgres:postgres@localhost:5432/alta
  --oracle_lib_dir ORACLE_LIB_DIR
                        Directory where oracle client libraries are installed (only needed when using oracle
                        connectors and the oracle libraries are not available on the path
  --connector CONNECTOR
                        Connector class containing the translation scheme from vo to adex
  --limit LIMIT         max records to fetch from VO, for dev/test purposes
  --batch_size BATCH_SIZE
                        number of records to post as a batch to ADEX
  --adex_backend_host [ADEX_BACKEND_HOST]
                        location of the adex-backend-django application
  --adex_resource ADEX_RESOURCE
                        resource/table to update, options are: primary_dp/create, ancillary_dp/create, activity/create
  --collection COLLECTION
                        can be used as filter in ADEX backend and ADEX frontend
  --clear_resource      Delete all the data from the adex_resource
  --clear_collection    Delete all the data for this collection from the adex_resource, works in concert with the '--
                        collection' parameter.
  --simulate_post       If true, then no data is posted to ADEX
  --adex_token ADEX_TOKEN
                        Token to login
  -v, --verbose         More information about atdb_services at run time.
  --argfile [ARGFILE]   Ascii file with arguments (overrides all other arguments
```

### STEP 3.2 - load data into ADEX
This deployment guide uses one of the example files to load a publicly available VO dataset into the ADEX cache database.

#### 3.2.1 - authentication token:
The only thing we need to change in the example file is the authentication token that **adex-data-scraper** uses to authenticate to **adex-backend-django**.

To generate and get that token (only once):
* go to the admin page at: http://localhost:8000/adex_backend/admin/
* choose 'tokens'
* choose 'add token' and select the user 'admin'
* choose 'SAVE'
* copy the token (key) to your clipboard


#### 3.2.2 - create argument file (your_datasource.args):
Assuming that you are still in the adex-data-scraper directory
* `cp examples/vo/apertif-dr1//apertif_dr1_continuum_images_localhost.args _your_datasource.args_`
* edit _your_datasource.args_ with your favorite editor, and change the token into the token that you just copied

The settings in the argument file tell **adex-data-scraper** to pull all the dataproducts from the **apertif_dr1.continuum_images** table at the ASTRON VO server and write them to a ADEX collection named **apertif-dr1**

The '_connector_' parameter in the argument file points to one of the Python classes that contains all the logic to read and restructure the data. Additional connectors can be added for every dataset.

```commandline
--datasource=vo
--connector=APERTIF_DR1.ContinuumImagesConnector
--data_host=https://vo.astron.nl/tap/apertif_dr1.continuum_images
--batch_size=1000
--adex_backend_host=http://localhost:8000/adex_backend/
--adex_resource=primary_dp/create
--adex_token=402dbaa6d0ac2d940429d5e24eddd7528a8244fd
--clear_collection
--collection=apertif-dr1
```

#### 3.2.3 - run adex-data-scraper
```commandline
> adex_data_scraper --argfile ./my_datasource.args

vo_scraper.run...
delete all records from primary_dp/create for collection apertif-dr1
1000 records fetched from https://vo.astron.nl/tap
1000 posted to http://localhost:8000/adex_backend/ in 0:00:03.848733
1000 records fetched from https://vo.astron.nl/tap
1000 posted to http://localhost:8000/adex_backend/ in 0:00:03.884310
1000 records fetched from https://vo.astron.nl/tap
1000 posted to http://localhost:8000/adex_backend/ in 0:00:03.956193
374 records fetched from https://vo.astron.nl/tap
374 posted to http://localhost:8000/adex_backend/ in 0:00:01.518156
3374 records in total in  0:00:19.864297
```

Now these dataproducts become visible in the ADEX Frontend as dataproducts of the **apertif**-dr1 collection.

![](adex_next2.png)


## deploying in Docker containers on test/production with CI/CD
The previous instructions describe a 'bare metal' installation for developers who work with the code.
In production the deployment mechanism is different, containerized (Docker) and highly automated with CI/CD pipelines from Gitlab. (similar as 'actions' on Github).

The 3 repositories (see the table in the 'architecture' section) all contain the following configuration files for this process.

* **Dockerfile**, to build the application into a docker image
* **docker-compose.yml**, to deploy and connect the applications, provide a reverse proxy (Traefik) and to provide the 'secrets'
* **.gitlab-ci.yml**, the CI/CD pipeline to test, stage and deploy the application on a desired host machine.

In production, ADEX is deployed in 3 Docker containers
* adex-backend-django
* adex-next frontend
* adex-cache database

