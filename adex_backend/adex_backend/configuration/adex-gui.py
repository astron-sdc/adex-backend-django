
# fields that can be queried on
fields = {
    "fields" : [
        {
            "field_id": 1,
            "name": "RA",
            "description": "Right Ascension in decimal degrees",
            "fieldType": "float",
            "isAdvancedFilter": False,
            "default" : 40.0,
            "extrema" : [0,360],
            "endpoint" : "api/v1/primary_dp/",
            "query_parameter": 'ra'
        },
        {
            "field_id": 2,
            "name": "Dec",
            "description": "Declination in decimal degrees",
            "fieldType": "float",
            "isAdvancedFilter": False,
            "default" : 40.0,
            "extrema" : [-90,90],
            "endpoint": "api/v1/primary_dp/",
            "query_parameter": 'dec'
        },
        {
            "field_id": 3,
            "name": "fov",
            "description": "Field of View ",
            "fieldType": "float",
            "isAdvancedFilter": False,
            "default": 30.0,
            "extrema" : [0,360],
            "endpoint": "api/v1/primary_dp/",
            "query_parameter": 'fov'
        },
        {
            "field_id": 4,
            "name": "Collection",
            "description": "Data collection",
            "fieldType": "string",
            "isAdvancedFilter": False,
            "default" : "focus",
            "endpoint": "api/v1/primary_dp/",
            "calc_values" : "get_distinct_values",
            "query_parameter": 'collection'
        },
        {
            "field_id": 5,
            "name": "Dataproduct Type",
            "description": "Type of the dataproduct",
            "fieldType": "String",
            "isAdvancedFilter": False,
            "default" : "image",
            "calc_values" : "get_distinct_values",
            "endpoint": "api/v1/primary_dp/",
            "query_parameter": 'dp_type'
        },
    ]
}

config = {
    "focus": {
        "defaults": {
            "ra": "40",
            "dec": "40",
            "fov": "30",
            "collection": { "name" : "apertif-dr1", "dp_types": ['science-skymap']},
            "dataproduct_type": "science-skymap",
            "selected_survey": 'DSS colored',
        },

        "collections": [
            { "name" : "linc_skymap", "dp_types": ['qa-skymap']},
            { "name" : "linc_visibilities", "dp_types": ['die-calibrated-visibilities'], "distinct_field" : "dataset_id"},
            { "name" : "apertif-dr1", "dp_types": ['science-skymap']},
            { "name" : "lotts-dr2", "dp_types": ['unknown']}
        ],

        "primary_dp_types": ['qa-skymap','science-skymap','die-calibrated-visibilities'],
        "ancillary_dp_types": ['diagnostic-plots','calibration-solution','pipeline-config','execution-log','summary'],
        "surveys": [
            {
                "name": "--- ASTRON HiPS ---",
                "selectable": False,
            },
            {
                "name": "apertif_dr1",
                "hips_name": "apertif_dr1",
                "hips_url": "https://hips.astron.nl/ASTRON/P/apertif_dr1",
                "format": "png"
            },
            {
                "name": "lotss_dr1_low",
                "hips_name": "lotss_dr1_low",
                "hips_url": "https://hips.astron.nl/ASTRON/P/lotss_dr1_low",
                "format": "png"
            },
            {
                "name": "lotss_dr1_high",
                "hips_name": "lotss_dr1_high",
                "hips_url": "https://hips.astron.nl/ASTRON/P/lotss_dr1_high",
                "format": "png"
            },
            {
                "name": "tgssadr",
                "hips_name": "tgssadr",
                "hips_url": "https://hips.astron.nl/ASTRON/P/tgssadr",
                "format": "png"
            },
            {
                "name": "lotss_dr2_low",
                "hips_name": "lotss_dr2_low",
                "hips_url": "https://hips.astron.nl/ASTRON/P/lotss_dr2_low",
                "format": "png"
            },
            {
                "name": "lotss_dr2_high",
                "hips_name": "lotss_dr2_high",
                "hips_url": "https://hips.astron.nl/ASTRON/P/lotss_dr2_high",
                "format": "png"
            },
            {
                "name": "--- Aladin HiPS ---",
            },
            {
                "name": "P/DSS2/color",
                "title": "DSS Colored (optical)"
            },
            {
                "name": "P/allWISE/color",
                "title": "allWISE (infrared)"
            },
            {
                "name": "P/XMM/PN/color",
                "title": "XMM PN colored"
            },
            {
                "name": "P/IRIS/color",
                "title": "IRIS color"
            },
            {
                "name": "P/Fermi/color",
                "title": "Fermi color"
            },
            {
                "name": "P/AKARI/FIS/Color",
                "title": "AKARI FIS Color"
            },
        ],
        "color_maps": ["native", "grayscale", "cubehelix", "eosb", "rainbow"],

        "backends": [
            {
                "name": "localhost:8000",
                "type": "primary_dp",
                "url": "http://localhost:8000",
            },
            {
                "name": "localhost WSL-2",
                "type": "primary_dp",
                "url": "http://localhost",
            },
            {
                "name": "SURF Research Cloud",
                "type": "primary_dp",
                "url": "http://145.38.187.31",
            },
            {
                "name": "sdc-dev.astron.nl",
                "type": "primary_dp",
                "url": "https://sdc-dev.astron.nl",
            },
            {
                "name": "sdc.astron.nl",
                "type": "primary_dp",
                "url": "https://sdc.astron.nl",
            },
        ],
        "labelfields": ['name', 'id', 'pid', 'dataset_id']
    },

}
