config = {
    "collections": [
        {"name": "linc_skymap", "dp_types": ["qa-skymap"]},
        {
            "name": "linc_visibilities",
            "dp_types": ["die-calibrated-visibilities"],
            "distinct_field": "dataset_id",
        },
        {"name": "apertif-dr1", "dp_types": ["science-skymap"]},
        {"name": "lotts-dr2", "dp_types": ["skymap"]},
        {"name": "alma_obscore", "dp_types": ['IMAGE', 'CUBE']},
        {"name": "hst_images", "dp_types": ['skyimage']}
    ],
    "skyview": {
        "surveys": {
            "ASTRON": [
                {
                    "id": "apertif_dr1",
                    "name": "apertif_dr1",
                    "url": "https://hips.astron.nl/ASTRON/P/apertif_dr1",
                    "format": "png",
                },
                {
                    "id": "lotss_dr1_low",
                    "name": "lotss_dr1_low",
                    "url": "https://hips.astron.nl/ASTRON/P/lotss_dr1_low",
                    "format": "png",
                },
                {
                    "id": "lotss_dr1_high",
                    "name": "lotss_dr1_high",
                    "url": "https://hips.astron.nl/ASTRON/P/lotss_dr1_high",
                    "format": "png",
                },
            ],
            "External": [
                {
                    "id": "P/DSS2/color",
                    "name": "P/DSS2/color",
                    "url": "https://healpix.ias.u-psud.fr/CDS_P_DSS2_color",
                    "format": "jpg",
                }
            ],
        }
    },
}
