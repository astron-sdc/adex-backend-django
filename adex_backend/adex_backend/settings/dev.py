from adex_backend.settings.base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEV = True
DEBUG = True

ALLOWED_HOSTS = ["*"]
CORS_ORIGIN_ALLOW_ALL = True

DATABASES = {
    'default': {
         'ENGINE': 'django.db.backends.postgresql_psycopg2',
         'NAME': 'adex_cache_django',
         'HOST': 'localhost',
         'PORT': '5432',
         'USER': 'postgres',
         'PASSWORD': 'postgres',
    },
}

# Password validation
# https://docs.djangoproject.com/en/1.10/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = []

DJANGO_CPROFILE_MIDDLEWARE_REQUIRE_STAFF = False
SILKY_PYTHON_PROFILER = True
SILKY_PYTHON_PROFILER_BINARY = True
SILKY_PYTHON_PROFILER_RESULT_PATH = 'profiles'