from adex_backend.settings.base import *

DATABASES = {
    'default': {
         'ENGINE': 'django.db.backends.postgresql_psycopg2',
         'USER': 'postgres',
         'PASSWORD': 'adex123',
         'NAME': 'adex-db',
         'HOST': 'postgres',
         'PORT': '5432',
    },
}
