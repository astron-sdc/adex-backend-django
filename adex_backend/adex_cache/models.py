from django.db import models

PARAMETER_MAPPING = {
    'ra' : 'ra',
    'dec' : 'dec',
    'dp_type' : 'dp_type',
    'collection': 'collection',
    'minimum_frequency' : None,
    'central_frequency' : 'central_frequency',
}

class SkyView(models.Model):

    pid = models.CharField(max_length=50)
    name = models.CharField(max_length=50, blank=True, null=True)
    ra = models.FloatField(db_index=True)
    dec = models.FloatField(db_index=True)
    observation = models.CharField(max_length=50, blank=True, null=True)
    beam = models.IntegerField()
    collection = models.CharField(max_length=30, blank=True, null=True)
    level = models.IntegerField()
    dataproduct_type = models.CharField(db_index=True,max_length=30)
    dataproduct_subtype = models.CharField(db_index=True,max_length=50)
    access_url = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return str(self.pid)


class Activity(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)
    dataset_id = models.CharField(max_length=40, blank=True, null=True) # SAS_ID

    # lofar_observation, lofar_pipeline, ldv_pipeline
    type = models.CharField(max_length=50, default='lofar_pipeline')
    url = models.CharField(max_length=255, blank=True, null=True)
    version = models.CharField(max_length=30, blank=True, null=True)

    # identifier for a specific dataset, this comes in through a CLI parameter from the data-scraper.
    # this makes it possible to import/remove per data-scraper run. Values like apertif-dr1,lotts-dr2,test-dataset
    collection = models.CharField(max_length=40, blank=True, null=True)

    # this makes it possible to show activities in a skyview,
    # and using them as such as aggregation layer.
    ra = models.FloatField(db_index=True, default=0)
    dec = models.FloatField(db_index=True, default=0)

    # parent = models.ForeignKey("self", null=True, blank=True, on_delete=models.SET_NULL)
    parents = models.ManyToManyField("self", blank=True)

    def __str__(self):
        return str(self.dataset_id) + ' - '  + str(self.name)


DP_TYPE_CHOICES = (
    ('die_calibrated_visibilities','die_calibrated_visibilities'),
    ('calibration_solutions','calibration_solutions'),
    ('diagnostic-plots','diagnostic-plots'),
    ('qa_summary','qa_summary'),
    ('science-skymap','science-skymap'),
    ('pipeline_config','pipeline_config'),
    ('log','log'),

    ('dde_calibrated_visibilities','dde_calibrated_visibilities'),
    ('science_skymap','science_skymap'),
    ('unknown','unknown'),
)

PRIMARY_FORMAT_CHOICES = (
    ('ms','ms'),
    ('hdf5','hdf5'),
    ('png','png'),
    ('jpg','jpg'),
    ('txt','txt'),
    ('fits','fits'),
    ('parset','parset'),
    ('other', 'other')
)

PRIMARY_LOCALITY_CHOICES = (
    ('unknown','unknown'),
    ('tape','tape'),
    ('online','online'),
)

class PrimaryDP(models.Model):

    pid = models.CharField(db_index=True, max_length=250)
    name = models.CharField(max_length=100)
    dataset_id = models.CharField(max_length=40, blank=True, null=True)

    dp_type = models.CharField(db_index=True, max_length=50, default='science-skymap')
    format = models.CharField(db_index=True,max_length=15, default='fits')
    locality = models.CharField(max_length=8, choices=PRIMARY_LOCALITY_CHOICES, default='online')
    access_url = models.CharField(max_length=255, blank=True, null=True)
    timestamp = models.DateTimeField(blank=True, null=True)

    ra = models.FloatField(db_index=True)
    dec = models.FloatField(db_index=True)
    fov = models.FloatField(blank=True, null=True)
    equinox = models.CharField(max_length=10, blank=True, null=True, default="2000.0")

    exposure_time = models.FloatField(blank=True, null=True)
    central_frequency = models.FloatField(blank=True, null=True)
    frequency_resolution = models.FloatField(blank=True, null=True)
    time_resolution = models.FloatField(blank=True, null=True)
    bandwidth = models.FloatField(blank=True, null=True)

    release_date = models.DateTimeField(blank=True, null=True)
    data_provider = models.CharField(max_length=40, blank=True, null=True)

    PSF_size = models.JSONField(blank=True, null=True)
    WCS = models.JSONField(blank=True, null=True)
    sky_footprint = models.TextField(blank=True, null=True)

    # identifier for a specific dataset, this comes in through a CLI parameter from the data-scraper.
    # this makes it possible to import/remove per data-scraper run. Values like apertif-dr1,lotts-dr2,test-dataset
    collection = models.CharField(max_length=40, blank=True, null=True)

    pipeline_url = models.CharField(max_length=255, blank=True, null=True)
    pipeline_version = models.CharField(max_length=30, blank=True, null=True)
    activity = models.ForeignKey(Activity, related_name='dataproducts', null=True, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.pid) + ' - ' + str(self.name)

    def save(self, *args, **kwargs):
        # intercept the save to check/save activities

        # check if this activity already exists
        try:
            activity = Activity.objects.get(dataset_id=self.dataset_id)
        except:
            # activity doesn't exist, create it, based on minimal metadata.
            # giving the activity the same ra,dec as the first dataproduct encountered may be a bit too rough,
            # this may need to become beam 0.
            activity = Activity(name = self.dataset_id,
                                dataset_id=self.dataset_id,
                                collection=self.collection,
                                url = self.pipeline_url,
                                version = self.pipeline_version,
                                ra = self.ra,
                                dec = self.dec)
            activity.save()

        self.activity = activity

        # continue with saving the primary dataproduct
        super(PrimaryDP, self).save(*args, **kwargs)

class AncillaryDP(models.Model):
    pid = models.CharField(db_index=True, max_length=250)
    name = models.CharField(max_length=100)
    dataset_id = models.CharField(max_length=40, blank=True, null=True)
    #dp_type = models.CharField(db_index=True, max_length=50, choices = DP_TYPE_CHOICES, default='diagnostic-plots')
    dp_type = models.CharField(db_index=True, max_length=50, default='diagnostic-plots')

    format = models.CharField(max_length=15, default='png')

    # this dataproduct could either be accessed by a url, or it can contain actual metadata... or both
    access_url = models.CharField(max_length=255, blank=True, null=True)
    timestamp = models.DateTimeField(blank=True, null=True)
    content = models.JSONField(blank=True, null=True)

    activity = models.ForeignKey(Activity, related_name='ancillary', null=True, on_delete=models.CASCADE)

    # identifier for a specific dataset, this comes in through a CLI parameter from the data-scraper.
    # this makes it possible to import/remove per data-scraper run. Values like apertif-dr1,lotts-dr2,test-dataset
    collection = models.CharField(max_length=40, blank=True, null=True)

    def __str__(self):
        return str(self.pid) + ' - ' + str(self.name)