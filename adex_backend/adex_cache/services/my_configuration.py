import os, sys
from django.conf import settings
import logging, importlib

from django.db.models import Min, Max
from ..models import SkyView, PARAMETER_MAPPING, PrimaryDP

logger = logging.getLogger(__name__)

try:
    sys.path.append(settings.CONFIGURATION_DIR)
    my_config = importlib.import_module(settings.CONFIGURATION_FILE)
except:
    # no configuration found, the frontend will have to use its defaults)
    pass


# return expanded configuration
def get_configuration(config_name=None, session=None):
    """
    Get the full configuration object from the configuration file
    :param config_name: name of the configuration file
    :return: the full configuration file, as json response
    """

    result = {}
    result['version'] = settings.API_VERSION
    #token = session.get("oidc_access_token", None)s

    # default the configuration from settings.CONFIGURATION_FILE is used (adex-gui),
    # but it can be overridden with the 'name' parameter like this
    # /adex-django-backend/adex-cache/configuration?name=adex-labs

    try:

        my_config = importlib.import_module(config_name,'adex_backend.configuration')

        # More sophisticated config files can embed the config in a class called Config and use the session
        try:
            my_config = my_config.Config(session)
        except AttributeError as e:
            # Config function is not required, so continue silently
            pass

    except Exception as error:
        return {"configuration error in ": config_name+ ".py : " + str(error) + " session => " + str(session.keys())}

    # add config object, if present
    try:
        result['config'] = my_config.config
    except:
        pass

    # add settings object, if present
    try:
        result['settings'] = my_config.settings
    except:
        pass

    # add groups object, if present
    try:
        result['groups'] = my_config.groups
    except:
        pass

    # add fields object, if present
    try:
        result['fields'] = my_config.fields
    except:
        pass

    return result


def get_settings(config_name=None):
    """
    Get only the 'settings' object from the configuration
    :param config_name: name of the configuration file
    :return: the 'settings' definition from the configuration file, as json response
    """

    result = {}

    try:
        my_config = importlib.import_module(config_name,'adex_backend.configuration')
        result['settings'] = my_config.settings
    except Exception as error:
        return {"configuration error in ": config_name+ ".py : " + str(error) }

    return result

def get_config(config_name=None):
    """
    Get only the 'config' object from the configuration
    :param config_name: name of the configuration file
    :return: the 'config' definition from the configuration file, as json response
    """

    result = {}

    try:
        my_config = importlib.import_module(config_name,'adex_backend.configuration')
        result['config'] = my_config.config
    except Exception as error:
        return {"configuration error in ": config_name+ ".py : " + str(error) }

    return result

def get_groups(config_name=None):
    """
    Get only the 'groups' object from the configuration
    :param config_name: name of the configuration file
    :return: the 'groups' definition from the configuration file, as json response
    """

    result = {}

    try:
        my_config = importlib.import_module(config_name,'adex_backend.configuration')
        result['groups'] = my_config.groups
    except Exception as error:
        return {"configuration error in ": config_name+ ".py : " + str(error) }

    return result


def get_fields(config_name=None):
    """
    Get only the 'fields' object from the configuration
    :param config_name: name of the configuration file
    :return: the 'fields' definition from the configuration file, as json response
    """

    result = {}

    try:
        my_config = importlib.import_module(config_name,'adex_backend.configuration')
        result['fields'] = my_config.fields
    except Exception as error:
        return {"configuration error in ": config_name+ ".py : " + str(error) }

    return result


def get_field_record(config_name, field_id):
    my_config = importlib.import_module(config_name, 'adex_backend.configuration')

    # retrieve the field from the configuration
    my_fields = my_config.fields

    for field in my_fields['fields']:
        if field['field_id'] == field_id:
            return field

def get_field_details(config_name, field_id):
    """
    Get details from this field, including extrema
    :param name: field_id, as specified in 'fields' configuration
    :return: the 'field' definition from the configuration file, as json response
    """

    result = {}

    try:
        field_record = get_field_record(config_name,field_id)
        logger.info("get_field_details for " + str(field_record))

        # retrieve the query_parameter to find the extremes
        query_parameter = field_record['query_parameter']

        # translate it to a database field to query
        try:
            field_to_query = PARAMETER_MAPPING[query_parameter]
        except:
            return {"ERROR": "query_parameter '"+ query_parameter + "', for field "+
                             str(field_id)+", is not mapped to a field in PARAMETER_MAPPING in models.py"}

        # query the database
        # TODO: make this more flexible and generic, it may not be the SkyViews model,
        #  but another table with ancillery data or something else.
        #  Should that logic be defined in the field definitions or hidden from that?
        try:
            # get all distinct values in the database for the given field
            if 'get_distinct_values' in  field_record['calc_values']:
                calc_values = PrimaryDP.objects.values_list(field_to_query, flat=True).distinct()
                field_record['distinct_values'] = list(calc_values)

            # get the min value in the database for the given field
            if 'get_min' in  field_record['calc_values']:
                query = field_to_query + '__min'
                calc_values = PrimaryDP.objects.all().aggregate(Min(field_to_query))[query]
                field_record['min'] = calc_values

            # get the max value in the database for the given field
            if 'get_max' in field_record['calc_values']:
                query = field_to_query + '__max'
                calc_values = PrimaryDP.objects.all().aggregate(Max(field_to_query))[query]
                field_record['max'] = calc_values

        except:
            # no 'calc_values' value found, continue without it.
            pass
        
        result = field_record
        
    except Exception as error:
        return {"configuration error in ": config_name+ ".py : " + str(error) }

    return result