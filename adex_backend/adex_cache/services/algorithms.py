"""
common helper functions
"""
import logging
import time
# from datetime import *

from astropy.coordinates import SkyCoord
from django.db import connection

from adex_cache.models import PrimaryDP

logger = logging.getLogger(__name__)


# this is a decorator that can be put in front (around) a function all to measure its execution time
def timeit(method):
    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()
        if 'log_time' in kw:
            name = kw.get('log_name', method.__name__.upper())
            kw['log_time'][name] = int((te - ts) * 1000)
        else:
            print('execution time: %r  %2.2f ms' % \
                  (method.__name__, (te - ts) * 1000))
        return result
    return timed


def get_sky_coords(target_name):
    """
    Convert object name to sky coordinates in degrees (up to 7 decimals)
    :return SkyCoord object. With example coordinates:
        <SkyCoord (ICRS): (ra, dec) in deg (98.4756375, 17.7702528)>
    """
    target_coords = SkyCoord.from_name(target_name)
    return target_coords


def add_cone_search_to_queryset(queryset, ra, dec, fov, collection, dp_type, cone=False):
    """
    query primary dataproducts that lie withing 'fov' degrees of 'ra' and 'dec'.
    Filter on collection and dp_type also.
    If 'cone=False', then a rectangle query is done.
    If 'cone=True', then a cone search with a stored procedure in the database is done.
    """

    if not cone:
        # TODO: obsolete this... after the next sprint demo, I still need it to demo the difference
        # rectangle query, translate ra,dec,fov to do a query
        # (which doesn't work nice at the poles or when stradling the 0-meridian)
        # example: adex_backend/api/v1/primary_dp/?&ra=40&dec=40&fov=30&collection=apertif-dr1&dp_type=science-skymap&page_size=10000
        radius = (fov / 2)
        ra_min = ra - radius
        ra_max = ra + radius
        dec_min = dec - radius
        dec_max = dec + radius
        results = queryset.filter(ra__gte=ra_min,ra__lte=ra_max,dec__gte=dec_min,dec__lte=dec_max)
    else:

        # cone search, by accessing a stored procedure in the database
        # example: adex_backend/api/v1/primary_dp/?&ra=40&dec=40&fov=30&collection=apertif-dr1&dp_type=science-skymap&cone=true
        radius = (fov / 2)

        ids_only = True
        if ids_only:
            with connection.cursor() as cursor:
                # execute the cone search procedure
                cursor.execute("CALL cone_search_ids(%s, %s, %s, %s, %s);", (ra, dec, radius, collection, dp_type))

                # results are stored in the database, fetch only the id.
                cursor.execute("SELECT id from cone_search_results;")
                data = cursor.fetchall()

                # construct a list of ids for the queryset filter
                list_of_ids=[]
                for record in data:
                    list_of_ids.append(record[0])

            # filter queryset
            # this will do another request to the database
            results = queryset.filter(id__in=list_of_ids)

        else:
            # WIP: trying to make it a single request.

            # uses the following stored function (only in dev)
            """
            CREATE OR REPLACE FUNCTION cone_search_primarydp(ra_center FLOAT, dec_center FLOAT, radius FLOAT, collection_filter character varying(40), dp_type_filter character varying(50))
            RETURNS SETOF adex_cache_primarydp
            AS $$
            
            DECLARE
                RAD2DEG FLOAT:= 57.2957795;
                DEG2RAD FLOAT:= 0.0174532925;
                row adex_cache_primarydp;
                distance_in_degrees FLOAT;
                
                BEGIN
            
                    FOR row IN (
                        SELECT * FROM adex_cache_primarydp
                    )
                    LOOP
                        -- input and output in decimal degrees
                        distance_in_degrees := haversine(row.ra, row.dec, ra_center, dec_center, COS(DEG2RAD * dec_center), SIN(DEG2RAD * dec_center));
                        
                        -- not only filter on distance, but also on collection and dp_type
                        if row.collection = collection_filter THEN
                            if row.dp_type = dp_type_filter THEN
                                IF distance_in_degrees <= radius THEN
                                    RETURN NEXT row;
                                END IF;
                            END IF;
                        END IF;
                    END LOOP;
                END;
            $$ LANGUAGE plpgsql;
            """

            # (1) this solution works, but adds nothing new compared to original mechanism
            # it does omit the temporary table in the database, but also sends more data over the line

            # with connection.cursor() as cursor:
                # execute the cone search procedure
                # cursor.execute("SELECT * FROM cone_search_primarydp(%s, %s, %s, %s, %s);", (ra, dec, fov, collection, dp_type))
                # rows = cursor.fetchall()

                # construct a list of ids for the queryset filter
                # list_of_ids = []
                # for row in rows:
                #    list_of_ids.append(row[0])

            # filter queryset
            # this will do another request to the database
            # results = queryset.filter(id__in=list_of_ids)

            # (2) the query works, a single request yielding a RawQuerySet... and then the trouble starts,
            #     because a RawQuerySet is not quite a QuerySet, and converting it hasn't worked (yet)...
            raw_results = PrimaryDP.objects.raw("SELECT * FROM cone_search_primarydp(%s, %s, %s, %s, %s);", (ra, dec, fov, collection, dp_type))

            # (3)
            # results = raw_results
            #  => AttributeError: 'RawQuerySet' object has no attribute 'all' (somewhere downstream in django-filters

            # (4)
            #results = PrimaryDP.objects.from_queryset(raw_results)
            # => AttributeError: 'RawQuerySet' object has no attribute '__name__'

            # (5)
            # instances = []
            # for row in raw_results:
            #     instances.append(row)
            #results = PrimaryDP.from_queryset(QuerySet(model=PrimaryDP, query=instances))
            # => AttributeError: 'QuerySet' object has no attribute '__name__'

    return results
