from django.urls import include, path
from django.views.generic import TemplateView
from rest_framework.schemas import get_schema_view
from . import views

urlpatterns = [
    # --- GUI ---
    path('', views.IndexView.as_view(), name='index'),

    # --- REST API ---
    # configuration
    path('configuration-esap/', views.ConfigurationESAPView, name='configuration-esap'),
    path('configuration/', views.ConfigurationView, name='configuration'),
    path('configuration/field/<int:field_id>', views.ConfigurationFieldView, name='configuration-field'),

    # queries
    path('api/v1/skyview/', views.SkyViewView.as_view(), name='skyview'),
    path('api/v1/skyview_json/', views.SkyViewJson, name='skyview-json'),

    path('api/v1/primary_dp/', views.PrimaryDPView.as_view(), name='primary-dp'),
    path('api/v1/primary_dp/<int:pk>/', views.PrimaryDPDetailsView.as_view(), name='primary-dp-details'),
    path('api/v1/primary_dp/create/', views.PrimaryDPCreateView.as_view(), name='create-primary-dps'),

    path('api/v1/ancillary_dp/', views.AncillaryDPView.as_view(), name='ancillary-dp'),
    path('api/v1/ancillary_dp/create/', views.AncillaryDPCreateView.as_view(), name='create-ancillary-dps'),

    path('api/v1/activity/', views.ActivityView.as_view(), name='activity'),
    path('api/v1/activity/<int:pk>/', views.ActivityDetailsView.as_view(), name='activity-details'),

    # --- function endpoints ---
    # example: /adex_backend/get-sky-coordinates/?target_name=m31
    path('get-sky-coordinates/', views.get_sky_coordinates, name='get-sky-coordinates'),
    path('clear-primary-dps/', views.clear_primary_dps, name='clear-primary-dps'),
    path('clear-ancillary-dps/', views.clear_ancillary_dps, name='clear-ancillary-dps'),
    path('clear-activities/', views.clear_activities, name='clear-activities'),

    path('docs/', TemplateView.as_view(
        template_name='adex_cache/swagger-ui.html',
        extra_context={'schema_url': 'openapi-schema'}
    ), name='swagger-ui'),
    path('openapi', get_schema_view(
        title="ADEX backend",
        description="ADEX backend endpoints",
        version="1.0.0"
    ), name='openapi-schema'),
]
