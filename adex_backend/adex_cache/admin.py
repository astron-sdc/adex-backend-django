from django.contrib import admin

from .models import PrimaryDP, AncillaryDP, Activity

admin.site.register(PrimaryDP)
admin.site.register(AncillaryDP)
admin.site.register(Activity)