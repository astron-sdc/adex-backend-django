# Manually created by Nico Vermaas on 28 april 2023

from django.db import migrations, models

SQL = """
CREATE OR REPLACE PROCEDURE cone_search_ids(ra_center FLOAT, dec_center FLOAT, radius FLOAT, collection_filter character varying(40), dp_type_filter character varying(50))
AS $$
DECLARE
        ROW record;
        distance FLOAT;
    BEGIN
        DROP TABLE IF EXISTS cone_search_results;
        CREATE TABLE cone_search_results(
            id BIGINT NOT NULL,
            ra DOUBLE PRECISION NOT NULL,
            dec DOUBLE PRECISION NOT NULL,
	    distance FLOAT
        );
 
        FOR ROW IN (
            SELECT id, ra, dec, collection, dp_type FROM adex_cache_primarydp
        )
        LOOP
            distance := haversine(ROW.ra, ROW.dec, ra_center, dec_center, COS(0.0174532925 * dec_center), SIN(0.0174532925 * dec_center));
            if ROW.collection = collection_filter THEN
                if ROW.dp_type = dp_type_filter THEN
                    IF distance <= radius THEN
                        INSERT INTO cone_search_results(id, ra, dec, distance)
                        VALUES (ROW.id, ROW.ra, ROW.dec, distance);
                    END IF;
                END IF;
            END IF;
        END LOOP;
    END;
$$ LANGUAGE plpgsql;
"""

class Migration(migrations.Migration):

    dependencies = [
        ('adex_cache', '0021_haversine_stored_function'),
    ]

    operations = [migrations.RunSQL(SQL)]
