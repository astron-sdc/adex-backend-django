# Generated by Django 4.1.3 on 2023-04-04 07:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('adex_cache', '0014_alter_ancillarydp_format'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ancillarydp',
            name='dp_type',
            field=models.CharField(db_index=True, default='diagnostic-plots', max_length=50),
        ),
        migrations.AlterField(
            model_name='primarydp',
            name='dp_type',
            field=models.CharField(db_index=True, default='science-skymap', max_length=50),
        ),
        migrations.AlterField(
            model_name='primarydp',
            name='exposure_time',
            field=models.FloatField(blank=True, null=True),
        ),
    ]
