# Manually created by Nico Vermaas on 28 april 2023

from django.db import migrations, models

SQL = """
CREATE OR REPLACE FUNCTION haversine(
    ra1 FLOAT,
    dec1 FLOAT,
    ra2 FLOAT,
    dec2 FLOAT,
    cosdec2 FLOAT,
    sindec2 FLOAT
) RETURNS FLOAT AS $$

DECLARE
    RAD2DEG FLOAT:= 57.2957795;
    DEG2RAD FLOAT:= 0.0174532925;
 
    -- performance boost when cosdec2 and sindec2 can be pre-calculated and given as parameters
    varCOS2 FLOAT:= COALESCE (cosdec2, COS(DEG2RAD * dec2));
    varSIN2 FLOAT:= COALESCE (sindec2, SIN(DEG2RAD * dec2));
  
    delta_ra FLOAT := RADIANS(ra2 - ra1);
    delta_dec FLOAT := RADIANS(dec2 - dec1);
    distance FLOAT := RAD2DEG * (ACOS(COS(DEG2RAD *(dec1)) * varCOS2 * COS(DEG2RAD *(ra2 - ra1)) + SIN(DEG2RAD * (dec1)) * varSIN2));

BEGIN
    -- return distance in decimal degrees
	RETURN distance;
END;

$$ LANGUAGE plpgsql;
"""

class Migration(migrations.Migration):

    dependencies = [
        ('adex_cache', '0022_cone_search_ids_stored_procedure'),
    ]

    operations = [migrations.RunSQL(SQL)]
