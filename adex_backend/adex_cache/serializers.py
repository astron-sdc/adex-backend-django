from abc import ABC

from rest_framework import serializers
from .models import SkyView, PrimaryDP, AncillaryDP, Activity

class SkyViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = SkyView
        fields = '__all__'


class PrimaryDPSerializer(serializers.ModelSerializer):

    class Meta:
        model = PrimaryDP
        fields = '__all__'

    def create(self, validated_data):
        instance = PrimaryDP(**validated_data)

        # check if an Activity object already exists, or needs to be created also
        instance.save()
        return instance


class AncillaryDPSerializer(serializers.ModelSerializer):
    class Meta:
        model = AncillaryDP
        fields = '__all__'

    def create(self, validated_data):
        instance = AncillaryDP(**validated_data)

        # check if an Activity object already exists, or needs to be created also
        instance.save()
        return instance


class ActivitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Activity
        fields = '__all__'