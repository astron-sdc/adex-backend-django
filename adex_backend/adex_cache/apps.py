from django.apps import AppConfig


class AdexCacheConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'adex_cache'
