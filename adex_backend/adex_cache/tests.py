import unittest

from django.test import Client
from django.urls import reverse
from parameterized import parameterized


class TestAPI(unittest.TestCase):
    client = Client()

    @parameterized.expand([
        ("m 87", 200),
        ("PSR%20J0633%2b1746", 200),
        ("PSR J0633+1746", 404),
        ("", 400)
    ])
    def test_get_sky_coordinates(self, target_name, expected_status_code):
        url = reverse("get-sky-coordinates") + "?target_name=" + target_name
        response = self.client.get(url)
        assert response.status_code == expected_status_code
