import logging

from astropy.coordinates.name_resolve import NameResolveError
from django.db.models import Q
from django.http import JsonResponse
from django.views.generic import ListView
from django_filters import rest_framework as filters
from rest_framework import generics
from silk.profiling.profiler import silk_profile

from .models import Activity, AncillaryDP, PrimaryDP, SkyView
from .serializers import (
    ActivitySerializer,
    AncillaryDPSerializer,
    PrimaryDPSerializer,
    SkyViewSerializer,
)
from .services import algorithms, my_configuration
from .services.algorithms import timeit

logger = logging.getLogger(__name__)


def get_filtered_skyview(request):
    dps = SkyView.objects.all()

    try:
        dataproduct_type = request.GET.get('dataproduct_type', None)
        dps = dps.filter(
                    Q(dataproduct_type__icontains=dataproduct_type))
    except:
        pass

    return dps

# ---------- GUI Views -----------
class IndexView(ListView):
    """
    This is the main view of ADEX-backend.
    """
    queryset = SkyView.objects.all()
    template_name = 'adex_cache/index.html'


# ---------- REST API views ----------

# --- configuration ---
def ConfigurationView(request, field_id = None):
    """
    returns a (named) configuration as a json object.
    """

    config_name = request.GET.get('config_name','adex-gui')
    block = request.GET.get('block',None)

    logger.info("config_name=" + str(config_name))

    try:
        # return only a specified block, or everything
        if block == 'fields':
            config_as_json = my_configuration.get_fields(config_name)
        elif block == 'groups':
            config_as_json = my_configuration.get_groups(config_name)
        elif block == 'settings':
            config_as_json = my_configuration.get_settings(config_name)
        elif block == 'config':
            config_as_json = my_configuration.get_config(config_name)
        else:
            # return everything
            config_as_json = my_configuration.get_configuration(config_name, request.session)

    except Exception as error:
        config_as_json = "ERROR in configuration: "+str(error) + " session => " + str(request.session)
        logger.error(config_as_json)

    return JsonResponse({'configuration': config_as_json})


def ConfigurationFieldView(request, field_id = None):
    """
    returns detailed information about a field as a json object.
    This is a separate view from the ConfigurationView,
    because this accesses the database to find field extrema, which could be an 'expensive' operation.
    """

    config_name = request.GET.get('config_name','adex-gui')

    logger.info("config_name name=" + str(config_name) + ",field_id="+ str(field_id))

    try:
        config_as_json = my_configuration.get_field_details(config_name, field_id)

    except Exception as error:
        config_as_json = "ERROR in configuration: "+str(error)
        logger.error(config_as_json)

    return JsonResponse({'configuration': config_as_json})


def ConfigurationESAPView(request):
    """
    returns a (named) configuration as a json object in the ESAP structure.
    This functionality may become obsolete later, but it is provided now for easier transitioning
    to a more ADEX specific configuration mechanism
    """

    name = request.GET.get('name','adex-gui-esap')
    logger.info("configuration name=" + str(name))

    try:
        config_as_json = my_configuration.get_configuration(name, request.session)

    except Exception as error:
        config_as_json = "ERROR in configuration: "+str(error) + " session => " + str(request.session)
        logger.error(config_as_json)

    return JsonResponse({'configuration': config_as_json})


# --- queries ---
class SkyViewFilter(filters.FilterSet):
    """
    Filters for the skyviews (test/benchmarking) endpoint
    """
    class Meta:
        model = SkyView

        fields = {
            'ra': ['gt','gte','lt','lte','contains'],
            'dec': ['gt', 'gte', 'lt', 'lte', 'contains'],
            'level': ['gt', 'gte', 'lt', 'lte', 'contains','exact','in'],
            'pid': ['exact', 'icontains'],
            'name': ['exact', 'icontains'],
            'observation': ['exact', 'icontains'],
            'collection': ['exact', 'icontains','in'],
            'dataproduct_type': ['exact', 'icontains','in'],
            'dataproduct_subtype': ['exact', 'icontains','in'],
        }


class SkyViewView(generics.ListAPIView):
    """
    skyviews (test/benchmarking) endpoint
    """
    queryset = SkyView.objects.all().order_by('id')
    serializer_class = SkyViewSerializer
    filterset_class = SkyViewFilter

    @timeit
    @silk_profile(name='SkyViewView')
    def list(self, request, *args, **kwargs):
        return super(SkyViewView, self).list(request, *args, **kwargs)

@timeit
@silk_profile(name='SkyViewJson')
def SkyViewJson(request):
    '''
    This view does not use the DRF serialization, because it should give the exact same response as fastapi (for comparison)
    :param request: adex_backend/api/v1/skyview_json/
    :return: {"id":275957,"pid":"pid-ARTS210422094_CB24_TAB01.fits","name":"ARTS210422094_CB24_TAB01.fits","ra":107.069166667,"dec":43.0399444444,"observation":"210422094","beam":0,"collection":"apertif-timeseries","level":0,"dataproduct_type":"timeSeries","dataproduct_subtype":"pulsarTimingTimeSeries","access_url":"cold:210422094/CB24/ARTS210422094_CB24_TAB01.fits"}

    '''
    try:
        limit = int(request.GET.get('limit', 1000))
    except:
        limit = 1000

    queryset = SkyView.objects.all()[:limit]
    results = []

    for dp in queryset:

        record = {
            'id': dp.id,
            'pid': dp.pid,
            'name': dp.name,
            'ra': dp.ra,
            'dec': dp.dec,
            'observation': dp.observation,
            'beam': dp.beam,
            'collection': dp.collection,
            'level': dp.level,
            'dataproduct_type': dp.dataproduct_type,
            'dataproduct_subtype': dp.dataproduct_subtype,
            'access_url': dp.access_url,
        }
        results.append(record)

    return JsonResponse(results, safe=False)


class PrimaryDPFilter(filters.FilterSet):
    """
    filters for the PrimaryDP endpoint
    """
    class Meta:
        model = PrimaryDP

        fields = {
            'id': ['exact'],
            'pid': ['exact', 'icontains'],
            'name': ['exact', 'icontains'],
            'format': ['exact', 'icontains', 'in'],
            'locality': ['exact'],
            'dp_type': ['exact', 'icontains', 'in'],

            'exposure_time': ['gt', 'gte', 'lt', 'lte', 'exact'],
            'central_frequency': ['gt', 'gte', 'lt', 'lte','exact'],
            'frequency_resolution': ['gt', 'gte', 'lt', 'lte', 'exact'],
            'time_resolution': ['gt', 'gte', 'lt', 'lte', 'exact'],
            'bandwidth': ['gt', 'gte', 'lt', 'lte', 'exact'],

            'dataset_id': ['exact', 'in'],
            'collection': ['exact', 'in'],
        }


class PrimaryDPView(generics.ListCreateAPIView):
    """
    PrimaryDP list endpoint
    """

    serializer_class = PrimaryDPSerializer
    filterset_class = PrimaryDPFilter

    #@timeit
    #@silk_profile(name='PrimaryDPView')
    def list(self, request, *args, **kwargs):
        return super(PrimaryDPView, self).list(request, *args, **kwargs)
    @timeit
    def get_queryset(self):
        # override get_queryset
        queryset = PrimaryDP.objects.all().order_by('id')

        # check if the 'distinct_field=' query parameter was added to the request
        distinct_field = self.request.GET.get('distinct_field')

        if distinct_field is not None:
            # if the distinct parameter is set, then the value gives the field to filter 'distinct' on.
            # example: distinct_field=dataset_id only returns record swith unique dataset_id's
            queryset = queryset.order_by(distinct_field).distinct(distinct_field)

        # cone search
        fov = self.request.GET.get('fov')

        # fov is given, go do a cone search
        if (fov):
            # cone=true/false (default).
            # for now, choose between cone or square, for evaluation purposes
            cone_parameter = self.request.GET.get('cone')
            cone = False
            if cone_parameter is not None:
                if cone_parameter == 'true':
                    cone = True

            ra = self.request.GET.get('ra')
            dec = self.request.GET.get('dec')
            collection = self.request.GET.get('collection')
            dp_type = self.request.GET.get('dp_type')

            queryset = algorithms.add_cone_search_to_queryset(
                queryset,
                float(ra), float(dec), float(fov),
                collection, dp_type,
                cone)

        return queryset

class PrimaryDPDetailsView(generics.RetrieveUpdateDestroyAPIView):
    """
    PrimaryDP details endpoint
    """
    queryset = PrimaryDP.objects.all()
    serializer_class = PrimaryDPSerializer


class PrimaryDPCreateView(generics.CreateAPIView):
    """
    view for bulk post to PrimaryDP
    """
    queryset = PrimaryDP.objects.all()
    serializer_class = PrimaryDPSerializer

    def get_serializer(self, *args, **kwargs):
        data = kwargs.get("data", {})
        if isinstance(kwargs.get("data", {}), list):
            kwargs["many"] = True

        return super(PrimaryDPCreateView, self).get_serializer(*args, **kwargs)

    @timeit
    @silk_profile(name='PrimaryDPCreateView')
    def post(self, request, *args, **kwargs):
        return super(PrimaryDPCreateView, self).post(request, *args, **kwargs)


class AncillaryDPFilter(filters.FilterSet):
    """
    filters for the AncillaryDP endpoint
    """
    class Meta:
        model = AncillaryDP

        fields = {
            'id': ['exact'],
            'pid': ['exact', 'icontains'],
            'name': ['exact', 'icontains'],
            'format': ['exact', 'icontains', 'in'],
            'dp_type': ['exact', 'icontains', 'in'],

            'dataset_id': ['exact', 'icontains', 'in'],
            'collection': ['exact', 'icontains', 'in'],
        }


class AncillaryDPView(generics.ListCreateAPIView):
    """
    AncillaryDP list endpoint
    """
    queryset = AncillaryDP.objects.all().order_by('id')
    serializer_class = AncillaryDPSerializer
    filterset_class = AncillaryDPFilter

    #@timeit
    #@silk_profile(name='AncillaryDPView')
    def list(self, request, *args, **kwargs):
        return super(AncillaryDPView, self).list(request, *args, **kwargs)


class AncillaryDPCreateView(generics.CreateAPIView):
    """
    view for bulk post to AncillaryDP
    """
    queryset = AncillaryDP.objects.all()
    serializer_class = AncillaryDPSerializer

    def get_serializer(self, *args, **kwargs):
        data = kwargs.get("data", {})
        if isinstance(kwargs.get("data", {}), list):
            kwargs["many"] = True

        return super(AncillaryDPCreateView, self).get_serializer(*args, **kwargs)

    @timeit
    @silk_profile(name='AncillaryDPCreateView')
    def post(self, request, *args, **kwargs):
        return super(AncillaryDPCreateView, self).post(request, *args, **kwargs)


class ActivityFilter(filters.FilterSet):
    class Meta:
        model = Activity

        fields = {
            'url': ['exact', 'icontains'],
            'version': ['exact', 'icontains'],
            'collection': ['exact', 'icontains', 'in'],
            'dataset_id': ['exact', 'icontains', 'in'],
        }


class ActivityView(generics.ListAPIView):
    queryset = Activity.objects.all().order_by('id')
    serializer_class = ActivitySerializer
    filterset_class = ActivityFilter

    # @timeit
    # @silk_profile(name='ActivityView')
    def list(self, request, *args, **kwargs):
        return super(ActivityView, self).list(request, *args, **kwargs)


class ActivityDetailsView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Activity.objects.all()
    serializer_class = ActivitySerializer


# ---------- Command endpoint views ----------
@silk_profile(name="get_sky_coordinates")
def get_sky_coordinates(request):
    """
    Uses astropy to retrieve RA,dec coordinates based on a 'target_name'
    :param request:
        Http request with 'target_name' as a query param.
        Example: /adex_backend/get-sky-coordinates/?target_name=m51
    :return HttpResponse
    """
    target_name = request.GET.get("target_name")
    if not target_name:
        return JsonResponse({"error": "no target_name given"}, status=400)

    logger.debug(f"get_sky_coordinates({target_name})")
    try:
        target_coords = algorithms.get_sky_coords(target_name)
        content = {
            "description": "ICRS (ra, dec) in deg",
            "target_name": target_name,
            "ra": target_coords.ra.deg,
            "dec": target_coords.dec.deg,
        }
        return JsonResponse(content)

    except NameResolveError:
        return JsonResponse(
            {"error": "could not resolve target_name"}, status=404
        )


# @permission_required('is_staff')
def clear_primary_dps(request):
    '''
    delete records from the PrimaryDP model
    '''
    collection = request.GET.get('collection',None)

    if collection:
        PrimaryDP.objects.filter(collection=collection).delete()
    else:
        PrimaryDP.objects.all().delete()

    logger.info('clear_primary_dps')

    return JsonResponse(
        {"result": "cleared primary dataproducts"}
    )


# @permission_required('is_staff')
def clear_ancillary_dps(request):
    '''
     delete records from the AncillaryDP model
    '''
    collection = request.GET.get('collection',None)

    if collection:
        AncillaryDP.objects.filter(collection=collection).delete()
    else:
        AncillaryDP.objects.all().delete()

    logger.info('clear_ancillary_dps')

    return JsonResponse(
        {"result": "cleared ancillary dataproducts"}
    )


# @permission_required('is_staff')
def clear_activities(request):
    '''
     delete records from the Activity model
    '''
    collection = request.GET.get('collection',None)

    if collection:
        Activity.objects.filter(collection=collection).delete()
    else:
        Activity.objects.all().delete()

    logger.info('clear_activities')

    return JsonResponse(
        {"result": "cleared activities"}
    )
